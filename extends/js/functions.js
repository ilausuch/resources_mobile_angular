/**
Check if a value is undefined, null or void
*/
function isNull(value){
	return value===undefined||value===null||value=="";
}

/**
Load script (js) and when it finished call a callback
*/
function loadScript(url, callback){
    var script = document.createElement("script");
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}


/**
Sincronizce several operations, when all finish call a callback
*/
function Sync(total,callback){
	this.count=0;
	this.total=total;
	this.callback=callback;
	
	this.reset=function(){
		this.count=0;
	}
	
	this.ready=function(){
		this.count++;
		if (this.count==this.total){
			callback();
		}
	}
}


function notifyMe(title,body,image) {
	var options={icon:image,body:body}
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) {
		alert("This browser does not support desktop notification");
	}
	
	// Let's check if the user is okay to get some notification
	else if (Notification.permission === "granted") {
	// If it's okay let's create a notification
		var notification = new Notification(title,options);
	}
	
	// Otherwise, we need to ask the user for permission
	// Note, Chrome does not implement the permission static property
	// So we have to check for NOT 'denied' instead of 'default'
	else if (Notification.permission !== 'denied') {
		Notification.requestPermission(function (permission) {
			// Whatever the user answers, we make sure we store the information
			if(!('permission' in Notification)) {
				Notification.permission = permission;
				}
	
			// If the user is okay, let's create a notification
			if (permission === "granted") {
				var notification = new Notification(title,options);
			}
		});
	}
	
	// At last, if the user already denied any notification, and you 
	// want to be respectful there is no need to bother him any more.
}

//Por probar
function clearHistory() {          
    var Backlen=history.length;
    if (Backlen > 0) history.go(-Backlen);
}


/**
* Show an alert notification (only firefox, others a simple alert)
*/
function firefoxNotification(message) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification(message);
  }

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
    	console.debug(permission);
      // Whatever the user answers, we make sure we store the information
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }

      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var notification = new Notification(message);
      }
    });
  }

  // At last, if the user already denied any notification, and you 
  // want to be respectful there is no need to bother them any more.
}
