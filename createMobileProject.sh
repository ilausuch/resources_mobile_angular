#!/bin/sh
die () {
    echo >&2 "$@"
    exit 1
}


[ "$#" -eq 1 ] || die "Params required provider.name (ej: ceu.incidencias). Provided $#"

[ ! -d $1 ] || die "Directory $1 allready exits"

type -P cordova || die "cordova executable doesn't exist"
type -P git || die "git executable doesn't exist"

echo "Creating $1 proyect..."
cordova create $1 com.$1 $1

cd $1

echo "Configuring android & ios..."
cordova platform add ios
cordova platform add android

echo "Installing plugins..."
cordova plugin add org.apache.cordova.dialogs
cordova plugin add org.apache.cordova.vibration
cordova plugin add org.apache.cordova.device
cordova plugin add org.apache.cordova.device-orientation
cordova plugin add org.apache.cordova.network-information
cordova plugin add de.appplant.cordova.plugin.local-notification && cordova prepare

echo "Preparing proyect files..."
cd www
rm -rf *
mkdir app
mkdir templates

echo "* index.html"
cat >index.html <<EOL
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, target-densitydpi=device-dpi" />
		<meta name="msapplication-tap-highlight" content="no" />
		<title>$1</title>
		
		<link rel="stylesheet" href="resources_mobile_angular/mobile_angular_ui/css/mobile-angular-ui-hover.min.css" />
		<link rel="stylesheet" href="resources_mobile_angular/mobile_angular_ui/css/mobile-angular-ui-base.min.css" />
		<link rel="stylesheet" href="resources_mobile_angular/mobile_angular_ui/css/mobile-angular-ui-desktop.min.css" />
		
		<script type="text/javascript" src="resources_mobile_angular/angular/js/angular.min.js"></script>
		<script type="text/javascript" src="resources_mobile_angular/angular/js/angular-route.min.js"></script>
		<script type="text/javascript" src="resources_mobile_angular/angular/js/angular-touch.min.js"></script>
		<script type="text/javascript" src="resources_mobile_angular/mobile_angular_ui/js/mobile-angular-ui.min.js"></script>
		
		<script type="text/javascript" src="resources_mobile_angular/extends/js/angular.js"></script>
		<script type="text/javascript" src="resources_mobile_angular/extends/js/functions.js"></script>
		<script type="text/javascript" src="resources_mobile_angular/extends/js/mobile.js"></script>
		
		<link rel="stylesheet" href="app/app.css" />
		<script type="text/javascript" src="app/app.js"></script>
	</head>
	
	<body ng-app="app" ng-controller="MainController as mc">
		<!-- Left sidebar -->
		<div class="sidebar sidebar-left" toggleable parent-active-class="sidebar-left-in" id="mainSidebar">
			<!-- TODO : Put here left sidebar or delete it -->
		</div>
		
		<!-- Right sidebar -->
		<div class="sidebar sidebar-right" toggleable parent-active-class="sidebar-right-in" id="rightSidebar" yield-to="rigthSidebar">
			<!-- TODO : Put here right sidebar or delete it -->
		</div>
		
		<!-- APP -->
		<div class="app">
			
			<!-- Header bar -->
			<div class="navbar navbar-app navbar-absolute-top">
				<!-- center for title-->
				<div class="navbar-brand navbar-brand-center" yield-to="title" style="color:black">
					$1
					<!-- TODO : Put here the title -->
				</div>
				
				<!-- left buttons-->
				<div class="btn-group pull-left" yield-to="mavbarLeft">
					<!-- TODO : Add here left buttons -->
				</div>
				
				<!-- right buttons-->
				<div class="btn-group pull-right" yield-to="navbarAction">
					<!-- TODO : Add here right buttons -->
				</div>
			</div>
		    
		    <!-- Body -->
			<div class="app-body">
				<ng-view class="app-content"></ng-view>
			</div>
		</div>
	</body>
</html>
EOL

echo "* app/app.css"
echo "" > app/app.css

echo "* app/app.js"
cat >app/app.js <<EOL
var app = angular.module('app', ["ngRoute","ngTouch","mobile-angular-ui"]);

//Routing configuration
app.config(function(\$routeProvider, \$locationProvider) {
	\$routeProvider.when('/',
		{templateUrl: "templates/main.html",controller: 'RouterController'});
	
	//TODO : Implement other path
});

//Route controller
app.controller('RouterController', function(\$rootScope,\$scope,\$location,\$route,\$routeParams,\$http) {
	//To access to path \$location.path();
    //To access to params \$routeParams;
    /*
    //TODO If it's necessary define conditions
    if ($location.path()=='/path_ejemplo'){
		//TODO value changes using mc variable
	}
	*/
});

var mc;
app.controller("MainController", function(\$rootScope,\$scope,\$timeout,\$http){
	mc=this;

	//TODO: attributes
	//this.var1=false;

	//TODO: functions
	this.f1=function(){
		//this.var1=true or mc.var1=true;
	}

	//Delclare init operations when device is ready
	mobile.onDeviceReady=function(){
		//TODO: Init operations
	}
	
	//TODO: Other operations

	//Application start
	mobile.start();
});

EOL

echo "* templates/main.html"
cat >templates/main.html <<EOL
<p>Hola mundo</p>
EOL

echo "Preparing resources"
git clone https://ilausuch@bitbucket.org/ilausuch/resources_mobile_angular.git

